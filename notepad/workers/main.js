const input = document.getElementById('input');
const result = document.getElementById('result');

const powerWorker = new Worker('power.js');

input.onchange = function(e) {
  const { target : { value } } = e;
  console.log('input is', value);
  powerWorker.postMessage(value);
}

powerWorker.onerror = function(e) {
  console.log('oh no');
};

powerWorker.onmessage = function(e) {
  console.log('from power worker', e.data);
  result.innerHTML = e.data;
};
