function delayPromise(delayMs) {
  return new Promise(resolve => {
    setTimeout(n => { return resolve('hello') } , delayMs);
  });
}
const DELAY_VALUE = 4000;
let p = delayPromise(DELAY_VALUE);

p.then((x) => { console.log('first then'); console.log(x) } );

setTimeout(() => {
  console.log('done second resting');
  p.then(x => { console.log('second then'); console.log(x); } );
}, 1000);

setTimeout(() => {
  console.log('done third resting');
  delayPromise(DELAY_VALUE).then(x => { console.log('third then'); console.log(x); } );
}, 3000);


Promise.all([
  delayPromise(2000).then(() => 1),
  delayPromise(3000).then(() => 2)
]).then((resultArray) => console.log('results: ' + resultArray))

Promise.race([
  delayPromise(2000).then(() => 1),
  delayPromise(1000).then(() => 2)
]).then((n) => console.log(n + ' won!'))
