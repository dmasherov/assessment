const fetch = require('node-fetch');

function delay(data, ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => { resolve(data) }, ms);
  });
}

function getFetch() { 
  return fetch('https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get')
}

const fetched = getFetch();

fetched
  .then(data => data.text())
  .then(data => delay(data, 5000))
  .then(data => { console.log('first'); return data })
  .then(console.log)

fetched
  .then(data => data.text())
  .then(data => delay(data, 3000))
  .then(data => { console.log('second'); return data })
  .then(console.log)
