import { ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-a',
	templateUrl: './a.component.html',
	styleUrls: ['./a.component.css']
})
export class AComponent implements OnInit {

	prop = 0.5;
	ngDoCheck(x) {
		console.log('in app-a: checking');
	}
	constructor(private changeDetector: ChangeDetectorRef) {
		setTimeout(() => {
			console.log('in app-a: trigger timeout');
			// 	this.prop = this.random();
		}, 1000);
	}

	ngOnInit() {
	}

	random() {
		return Math.random();
	}

	ceil(value) {
		return Math.ceil(value);
	}

	call() {
		console.log('in app-a: change detection');
		return undefined;

	}
}
