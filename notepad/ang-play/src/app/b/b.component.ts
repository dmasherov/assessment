import { ChangeDetectionStrategy, Input, Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-b',
	templateUrl: './b.component.html',
	styleUrls: ['./b.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush

})
export class BComponent implements OnInit {

	@Input()
	dataForB;

	changedInTimeoutProperty;

	constructor() {
		setTimeout(() => {
			console.log('in app-b: trigger timeout');
			this.dataForB = 123;
			this.changedInTimeoutProperty = 123;
		}, 2000);
	}

	ngDoCheck() {
		console.log('in app-b: checking');
	}


	ngOnInit() {
	}

}
