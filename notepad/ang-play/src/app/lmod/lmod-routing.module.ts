import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LComponent } from './l.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
       path: 'l',
       component: LComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LmodRoutingModule { }
