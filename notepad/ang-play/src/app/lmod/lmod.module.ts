import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LComponent } from './l.component';
import { LmodRoutingModule } from './lmod-routing.module';

@NgModule({
  declarations: [LComponent],
  imports: [
    CommonModule,
    LmodRoutingModule,
  ]
})
export class LmodModule { }
