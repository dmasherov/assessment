// write add function with and without closures
function add(x, y) {
  if (!y) return y => x + y;
  return x + y;
}

console.log(add(1,2));
console.log(add(4)(2));;

// write fold function
function fold(fn, cur, arr) {
  if (!arr.length) return cur;
  const first = arr.shift();
  return fold(fn, fn(cur, first), arr);
}
console.log(fold(add, 0, [4, 2, 3]));


// write map function with fold
function map(fn, arr) {
  return fold((acc, el) => [...acc, fn(el)], [], arr);
}

console.log(map(add(1), [5,6,7]));


