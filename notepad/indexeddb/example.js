const request = indexedDB.open('MyExample', 5);

let db;
request.onsuccess = event => {
  db = event.target.result;
  console.log('open success', event, db);
  redraw();
};
request.onerror = event => {
  console.log('open error', event);
};

const orders = [
  {id: 1, name: 'aaa' },
  {id: 2, name: 'bbb' },
  {id: 3, name: 'ccc' },
  {id: 4, name: 'ddd' },
];
request.onupgradeneeded = event => {
  db = event.target.result;
  try {
    db.deleteObjectStore('orders');
  }
  catch (ignore) {}
  const objectStore = db.createObjectStore('orders', { keyPath: 'id' });
  objectStore.transaction.oncomplete = e => {
    console.log('open upgrade needed', event);
    const store = db.transaction('orders', 'readwrite').objectStore('orders');
    orders.forEach(order => {
      store.add(order);
    });
  };
};

const orderList = document.getElementById('order-list');
const removeId = document.getElementById('remove-id');

function redraw() {
  const getAllRequest = db.transaction('orders', 'readonly').objectStore('orders').getAll();
  getAllRequest.onsuccess = e => {
    const orderObjects = e.target.result;
    console.log(orderObjects);
    orderList.innerHTML = '';
    orderObjects.forEach(order => {
      const orderItem = document.createElement('li');
      orderItem.textContent = `Order id ${order.id}, name ${order.name}`;
      orderList.append(orderItem);
    });
  }
}

function remove() {
  const orderId = parseInt(removeId.value);
  console.log(orderId);
  const orderStore = db.transaction('orders', 'readwrite').objectStore('orders');
  const getRequest = orderStore.get(orderId);
  getRequest.onsuccess = function (event) {
    console.log('get success', getRequest.result, event.target, event.target.result);
  }
  const deleteRequest = orderStore.delete(orderId);
  deleteRequest.onsuccess = function (event) {
    console.log('deleteed!!!', event.target);
    redraw();
  };
}



