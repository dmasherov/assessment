const { throwError, Observable, Subject, interval, defer, of } = require('rxjs');
const { switchMap, take, refCount, publish, map, tap, share, shareReplay } = require('rxjs/operators');


const obs = new Observable((observer) => {
	observer.next(Math.random());
});

piped = obs.pipe(publish(), refCount());
piped = obs.pipe(shareReplay());

piped.subscribe((val) => console.log('first', val));
piped.subscribe((val) => console.log('second', val));


const pr = interval(1000).pipe(
	take(2),
	switchMap(() => throwError('1'))
).toPromise();
pr.then(() => console.log('promise!')).catch((error) => console.log('reject! ' + error));


const s = Reflect.construct(Subject, {});
s.subscribe((a) => console.log('subject', a));
s.next(1);
s.complete();
s.next(2);
