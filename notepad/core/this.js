'use strict';

var a = 1;
function f() {
  console.log(this.a);
}

f.apply({a: 2});

const r = f.bind({a: 333});
const y = r.bind({a:432});
r();
y();
