function protoChain(obj) {
  const chain = []
  let cur = obj;
  while (cur) {
    chain.push({ cur, isRoot: cur == Object.prototype });
    cur = Object.getPrototypeOf(cur);
  }
  return chain;
}

const a1 = new Number(1);
const a2 = new Number(2);
const a3 = new Number(3);

Object.setPrototypeOf(a3, a2);
Object.setPrototypeOf(a2, a1);

console.log(protoChain(a3));
