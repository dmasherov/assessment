const iterable = {
  i: 0,

  [Symbol.iterator]() {
    console.log('hi');
    return {
      i: this.i,
      next() {
        return { value: ++this.i, done: this.i >= 5}
      }
    }
  }
}

for (let x of iterable) {
  console.log(x);
}
