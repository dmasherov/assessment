'use strict';

const l = {
  x: 1,
  getX() {
    return this.x;
  },
  multiplied: function(n) {
    return n * this.x;
  },
  g: (z) => {
    console.log('in g', this);
    console.log('in g', x);
    console.log('in g', z);
  }
}

var x = 123;
console.log(l);
console.log(l.getX());
console.log(l.multiplied(2));
console.log(l.multiplied.bind({x: 4})(4));
console.log(l.g(1));
console.log(l.g.bind(l)(1));
