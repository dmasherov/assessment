'use strict';

function f(n) {
  console.log(n);
  console.log('arguments object', arguments);
  // console.log('callee', arguments.callee); fails in strict
  console.log('caller', arguments.caller);
  console.log('length', arguments.length);

  console.log('----------------------------------------');
  for (let a of arguments) {
    console.log('argument', a);
  }
}


function g(x, ...rest) {
  console.log('x', x);
  console.log('rest', rest);
  console.log('arguments object', arguments);
}


f(0)
f(1, 2, 3)

console.log('----------------------------------------');
console.log('calling g');

g(1);
g(1, 2);
g();
