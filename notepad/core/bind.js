function bind(fn, context) {
  return function() {
    return fn.apply(context, arguments);
  }
}

function gg() {
  return this.a;
}

console.log(gg.call({a: 1}));

const bindedGg = bind(gg, {a: 2});
console.log(bindedGg());


const unboundSlice = Array.prototype.slice;
const slice = Function.prototype.call.bind(unboundSlice);
const mySlice = bind(Function.prototype.call, unboundSlice);

console.log(slice([1,2,3], 1));
console.log(mySlice([1,2,3], 1));

